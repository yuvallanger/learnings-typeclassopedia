module Typeclassopedia where

import Prelude
	( Int
	, undefined
	)

class Functor f where
    fmap :: (a -> b) -> (f a) -> (f b)

data Either e a = Left e | Right a

instance Functor (Either e) where
    fmap f (Left e) = Left e
    fmap f (Right a) = Right (f a)

instance Functor ((->) e) where
    fmap ab ea = \e -> ab (ea e)

instance Functor ((,) e) where
    fmap ab (e,a) = (e, ab a)

data Pair a = Pair a a

instance Functor Pair where
    fmap ab (Pair a1 a2) = Pair (ab a1) (ab a2)

instance Functor [] where
	fmap ab [] = []
	fmap ab (a:as) = ab a : fmap ab as

data ITree a
	= Leaf (Int -> a)
	| Node [ITree a]

instance Functor ITree where
	fmap ab (Leaf ia) = Leaf (\i -> (ab (ia i)))
	fmap ab (Node ias) = Node (fmap (fmap ab) ias)

data NotFunctor a = NotFunctor (a -> Int)

instance Functor NotFunctor where
	fmap ab ai = NotFunctor (\b -> _)
